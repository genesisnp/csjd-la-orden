<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-banner scroll" id="parallax">
        <div class="degrade-int"></div>
        <img class="img-banner" src="assets/images/banner/red.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase wow fadeInLeft">FORMACIÓN</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic formation">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <!--  QUE ES -->
                            <div class="col-xs-12 pd-x-0">
                                <h2 class="titles-descrip mb-0 wow fadeInLeft"><span class="icon-san"></span>
                                    <span class="span-titlesDescrip">
                                    pastoral de</span><br>la salud social</h2>
                            </div>
                            <div class="col-xs-12 pd-x-0 q-es pr-3">
                                <div class="row">
                                    <div class="col-xs-12 col-md-5 descrip-formation wow fadeInLeft">
                                        <div class="row">
                                            <div class="col-xs-12 pd-x-0">
                                                <h2 class="sub-ttl-flotant color-primary">¿Qué es?</h2>
                                            </div>
                                            <p class="font-semi-bold text-justify subttl-int">Es un servicio de humanización y de buena noticia a
                                            favor de los enfermos y necesitados, a través de las palabras, actitudes y gestos terapéuticos 
                                            en la atención integral que se les brinda. </p>
                                            <p class="text-p2 text-justify">Es una gestión de nuestra Curia Provincial, en nombre de la Iglesia 
                                                y en continuidad con la misión salvadora de Cristo en el mundo de la salud, encaminada a orientar 
                                                y animar la organización y funcionamiento del modelo de Pastoral de la Salud y Social.</p>
                                            <p class="text-p2 text-justify">Fundamentado en la Biblia, en el carisma de San Juan de Dios y, en 
                                                los cambios actuales del proceso salud, en relación a todos nuestros Centros.</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <div class="content-img-pstoral">
                                            <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/pastoral-de-la-salud1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--MISION-->
                            <div class="col-xs-12 pd-x-0 mission-pastoral">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="content-img-pstoral float-left">
                                            <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/pastoral-de-la-salud2.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 descrip-formation wow fadeInRight">
                                        <div class="row">
                                            <div class="col-xs-12 pd-x-0">
                                                <h2 class="sub-ttl-flotant color-primary">Misión</h2>
                                            </div>
                                            <p class="font-semi-bold text-justify subttl-int">Facilita y canaliza la expresión de la dimensión religiosa 
                                                como fuente de salud y de vida para las personas.</p>
                                            <p class="text-p2 text-justify">Atender las necesidades espirituales y religiosas de los enfermos y necesitados, 
                                                de sus familias y de los propios profesionales de los Centros, iniciando procesos de acompañamiento y cercanía, 
                                                en los que la relación de ayuda adquiere una especial relevancia.</p>
                                            <p class="text-p2 text-justify">Todo ello, se ofrece desde el más profundo respeto a las creencias y valores de 
                                                las personas y a las diferentes confesiones religiosas.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- VISION -->
                            <div class="col-xs-12 pd-x-0">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="vission-pastoral row">
                                            <div class="img-vission-past col-xs-12 col-md-11 pd-x-0 hidden-xs hidde-sm">
                                                <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/pastoral-de-la-salud3.jpg" alt="">
                                            </div>
                                            <div class="description-flotant descp-flot-l wow fadeInLeft">
                                                <h2 class="sub-ttl-flotant color-primary">Visión</h2>
                                                <p class="text-p2">Ser reconocida por la animación de un modelo de gestión de beneficencia solidaria 
                                                    y por su aporte terapéutico, que libera las capacidades secuestradas del enfermo por condición de 
                                                    vida infrahumana</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- LINEAS MAESTRAS -->
                            <div class="col-xs-12 pd-x-0 lineas-maestras">
                                <div class="row">
                                    <div class="img-lineas-maestras col-xs-12 col-sm-10 pd-x-0">
                                        <img class="img-cover wow fadeInUp" src="assets/images/internas/formacion/lineas-maestras.jpg" alt="">
                                    </div>
                                    <div class="col-xs-12 col-md-4 wrapper-slide-lineas-m wow fadeInRight">
                                        <div class="m-slide">
                                            <h2 class="sub-ttl-flotant color-primary">Líneas maestras<br>de acción pastoral</h2>
                                            <div class="slide-lineas-maestras owl-carousel">
                                                <div class="text-p2">Tener una visión positiva del ser humano al margen de 
                                                    su conducta por ser imagen de Dios y, por ende, una proyección de todas sus perfecciones. 
                                                    Por eso, están presentes en toda persona sus tendencias innatas a la bondad, a la belleza 
                                                    y a la verdad. La búsqueda de la felicidad es connatural.</div>
                                                <div class="div text-p2">Brindar salud a la materialidad biológica enferma y todos sus aspectos de 
                                                    índole biográfico: los dinamismos más profundos del ser, sus relaciones interpersonales, las 
                                                    relaciones significativas y el sentido de la vida, y que en conjunto adquieren un rostro que 
                                                    son los anhelos de salvación. </div>
                                                <div class="div text-p2">Desarrollar hospitales con sabor a utopía, para que contraste fuertemente con 
                                                    la racionalidad humana en referencia a modelo de servicios de salud y gestión de los centros; que 
                                                    nuestras obras hospitalarias sean faros para iluminar caminos nuevos de asistencia y de humanidad.</div>
                                                <div class="div text-p2">Fomentar la antigua práctica mendicante que animan gestos, obras, acciones de 
                                                    solidaridad y de corresponsabilidad; y desde esta comunión de bienes, que hace más humana la vida 
                                                    de los hombres, crear las estructuras justas como condición para la cohesión social. </div>
                                                <div class="div text-p2">Suscitar al ser que sufre la convicción del valor de su persona. Capacitar en la 
                                                    inclusión económica y social; brindar rehabilitación que conlleve a la máxima independencia posible en 
                                                    el uso de sus facultades y, subrayar que en la atención prima la dignidad, el bienestar y el 
                                                    desarrollo personal. </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- DIMENSIONES -->
                            <div class="col-xs-12 pd-x-0">
                                <h2 class="titles-descrip mb-0 wow fadeInLeft"><span class="icon-san"></span>
                                    <span class="span-titlesDescrip">
                                    dimensiones de</span><br>pastoral de la salud</h2>
                            </div>
                            <!-- DIMENSIÓN PROFÉTICA -->
                            <div class="col-xs-12 pd-x-0 margin-dimens">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4 descrip-formation wow fadeInRight">
                                        <div class="row">
                                            <div class="col-xs-12 pd-x-0">
                                                <h2 class="sub-ttl-flotant color-primary">Dimensión Profética</h2>
                                            </div>
                                            <p class="text-p2 text-justify">La Orden Hospitalaria de San Juan de Dios (Roma 2012) señala que: 
                                            “Una Pastoral arraigada en la Biblia también incluye la dimensión profética. Dicha pastoral está caracterizada por 
                                            la denuncia valiente, por una acción práctica coherente y por el compromiso activo en aras de la justicia, siguiendo 
                                            el ejemplo de Jesús….”leyendo el futuro según la mirada de Dios” (carta de Identidad 8.2)…Levantará su voz cuando esté 
                                            en peligro la dignidad humana, se comprometerá por la justicia social y acogerá el desafío de su renovación constante 
                                            para responder a las exigencias siempre distintas de las situaciones y de los tiempos.</p>
                                            <p class="text-p2 text-justify">“El espíritu del señor esta sobre mí; Él me ha ungido para anunciar la buena nueva a 
                                            los pobres y a los asistidos” (Lc. 4,18).  La Iglesia, convocada por la Palabra, tiene como una de sus tareas principales 
                                            anunciar el evangelio de la vida y de la salud, la Pastoral de Salud y Social, integra la solidaridad con los pobres y 
                                            con los asistidos.</p>
                                            
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="content-img-pstoral float-right">
                                            <img class="img-cover wow fadeInUp" src="assets/images/internas/formacion/dimen-past1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <!-- DIMENSIÓN LITÚRGICA -->
                             <div class="col-xs-12 pd-x-0 margin-dimens">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="content-img-pstoral float-left">
                                            <img class="img-cover wow fadeInUp" src="assets/images/internas/formacion/dimen-past2.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 descrip-formation wow fadeInRight">
                                        <div class="row">
                                            <div class="col-xs-12 pd-x-0">
                                                <h2 class="sub-ttl-flotant color-primary">Dimensión Litúrgica</h2>
                                            </div>
                                            <p class="text-p2 text-justify">Es toda la actividad que la Iglesia realiza 
                                            orgánicamente en lo que respecta a su dimensión celebrativa-sacramental del 
                                            Misterio Pascual de Cristo.</p>
                                            <p class="text-p2 text-justify">Por medio de la liturgia nosotros los cristianos, 
                                            celebramos la abundante vida que nos regala Dios, de esta forma, la liturgia nos 
                                            estimula a celebrar la acción sanante de nuestro Padre Dios, que nos invita a 
                                            disfrutar plenamente la vida y nos sostiene en los momentos de enfermedad o de duelo. 
                                            Por otra parte, alimenta la vida espiritual de nuestros colaboradores, para que en 
                                            el ejercicio de su profesión sepan conjugar competencia y hospitalidad; además para 
                                            que con su dedicación y servicio sigan testimoniando la ternura de Dios y asegurando 
                                            condiciones de vida dignas. </p>
                                            <p class="text-p2 text-justify">La sagrada liturgia es lo central en la vida de la Iglesia 
                                            y de cada cristiano porque en ella celebramos los misterios de nuestra redención. El misterio
                                             principal es el misterio pascual que incluye el sufrimiento, muerte y resurrección de Nuestro 
                                             Señor Jesucristo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- DIMENSIÓN CARITATIVA -->
                            <div class="col-xs-12 pd-x-0 margin-dimens">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4 descrip-formation wow fadeInRight">
                                        <div class="row">
                                            <div class="col-xs-12 pd-x-0">
                                                <h2 class="sub-ttl-flotant color-primary">Dimensión Caritativa</h2>
                                            </div>
                                            <p class="font-semi-bold text-justify subttl-int">“La dimensión social de nuestra fe nos lleva a obrar con el mismo amor misericordioso 
                                            del Padre, actuando en los frentes del anuncio, de la denuncia y del testimonio.”</p>
                                            <p class="text-p2 text-justify">En la Carta de Identidad se nos afirma que: “San Juan de Dios se ha encarnado en los pobres 
                                            y en los enfermos como uno más, acogiéndolos y atendiendo a sus necesidades. Los ha curado, a pesar de sus límites, con las 
                                            riquezas del carisma de la hospitalidad que Dios le ha dado. Nunca se negó a ayudar a los necesitados con todo lo que podía 
                                            disponer en su pobreza.</p>
                                            <p class="text-p2 text-justify">Es importante aclarar que esta caridad se da hacia la interna de su propia obra con los 
                                            enfermos que atendía, así como aquellos que encontraba en su camino, es de esta manera que debemos concebir una labor caritativa.</p>
                                            
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="content-img-pstoral float-right">
                                            <img class="img-cover wow fadeInUp" src="assets/images/internas/formacion/dimen-past1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- DIMENSIÓN HOSPITALARIA -->
                            <div class="col-xs-12 pd-x-0 margin-dimens">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="content-img-pstoral float-left">
                                            <img class="img-cover wow fadeInUp" src="assets/images/internas/formacion/dimen-past4.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 descrip-formation wow fadeInRight">
                                        <div class="row">
                                            <div class="col-xs-12 pd-x-0">
                                                <h2 class="sub-ttl-flotant color-primary">Dimensión Hospitalaria</h2>
                                            </div>
                                            <p class="font-semi-bold text-justify subttl-int">La hospitalidad es este movimiento de acercamiento y acogida al otro, 
                                                que es vital para el progreso y la supervivencia.</p>
                                            <p class="text-p2 text-justify">La Hospitalidad según el estilo de nuestro Fundador lo encontramos detalladamente explicado 
                                                en las Constituciones en los numerales del 20 al 25. </p>
                                            <p class="text-p2 text-justify">Allí se señala que el origen de la hospitalidad está en la vida de Jesús de Nazaret quien 
                                                anuncia la Buena Noticia a los pobres y sana a los enfermos, con el amor misericordioso de Dios Padre con predilección 
                                                por los débiles, enfermos y pecadores.  “Evangelizar a través de la hospitalidad es lo específico de la Orden, practicar 
                                                la hospitalidad según el modelo ejemplar de San Juan de Dios significa evangelizar.</p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-formation.php'
            ?>
        </div>
    </section>
</main>
<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>