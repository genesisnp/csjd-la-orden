<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-banner scroll" id="parallax">
        <div class="degrade-int"></div>
        <img class="img-banner" src="assets/images/banner/reunete-a-nosotros.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase wow fadeInLeft">únete a nosotros</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
 
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-unete.php'
            ?>
        </div>
    </section>
</main>
<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>