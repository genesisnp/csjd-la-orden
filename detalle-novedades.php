<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-news detail-news">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ttl-news color-primary text-center titles-big"><h2>NOVEDADES</h2></div>
                <a href="novedades.php" class="icon-icono-regresar text-center disp-block"></a>

                <div class="col-xs-12 description-news">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="img-detail-news">
                                <img class="img-cover" src="assets/images/noticia1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="title-news-info">
                                <div class="info-news">
                                    <img class="icons-country-descr" src="assets/images/icons/peru.svg" alt="">
                                    <h2 class="font-nexaheavy ttl-descrip-news">Encuentro de Superiores Provinciales en Curia General - Roma</h2>
                                </div>
                                <p class="text-p2 p-fecha">26 de febrero del 2020</p>
                            </div>
                            <div class="text-info-news">
                                <p class="text-p2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae sit commodi nesciunt quae, dignissimos corporis amet tempore ex voluptatibus impedit ea odit quod. Temporibus odio dolorum autem, omnis facilis porro?</p>
                                <p class="text-p2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae sit commodi nesciunt quae, dignissimos corporis amet tempore ex voluptatibus impedit ea odit quod. Temporibus odio dolorum autem, omnis facilis porro?</p>
                                <p class="text-p2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae sit commodi nesciunt quae, dignissimos corporis amet tempore ex voluptatibus impedit ea odit quod. Temporibus odio dolorum autem, omnis facilis porro?</p>
                                <p class="text-p2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae sit commodi nesciunt quae, dignissimos corporis amet tempore ex voluptatibus impedit ea odit quod. Temporibus odio dolorum autem, omnis facilis porro?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</main>

<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>