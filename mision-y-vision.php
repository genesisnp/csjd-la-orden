<?php
    include 'src/includes/header.php'
?>
<main>
    <!--BANNER-->
    <section class="sct-banner scroll" id="parallax">
        <div class="degrade-int"></div>
        <img class="img-banner" src="assets/images/banner/red.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase wow fadeInLeft">LA ORDEN HOSPITALARIA</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip wow fadeInLeft"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                nuestra</span><br>misión y visión</h2>
                                    </div>
                                    <div class="col-xs-12 pd-x-0">
                                        <div class="row content-mv pr-3">
                                            <div class="col-xs-12 col-md-4 wrapper-mv">
                                                <div class="row">
                                                    <div class="mission wow fadeInLeft">
                                                        <h2 class="sub-ttl-flotant color-primary">Misión</h2>
                                                        <p class="text-internas text-justify">Somos una Institución Religiosa de confesionalidad 
                                                            católica que promueve la ayuda a los más necesitados, brindando Hospitalidad con la mejor 
                                                            atención en salud y una asistencia integral, humanizada y de calidad. </p>
                                                    </div>
                                                    <div class="vission wow fadeInLeft">
                                                        <h2 class="sub-ttl-flotant color-primary">Visión</h2>
                                                        <p class="text-internas text-justify">Ser el modelo de excelencia en Gestión Hospitalaria 
                                                            en nuestras Clínicas, Hospitales, Fundaciones y Centros de acogida. </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-md-8">
                                                <div class="img-mv-oh">
                                                    <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/mision-vision.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-oh.php'
            ?>
        </div>
    </section>
</main>
<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>