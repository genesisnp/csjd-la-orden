<?php
    include 'src/includes/header.php'
?>
<main id="fullpage" class="main-home">
    <!--CARROUSEL HOME-->
    <section class="section sct-carousel-home pos-rel container-fluid">
        <div class="slider-home hg-100 owl-carousel">
            <div class="item hg-100">
                <div class="degrade-int"></div>
                <video autoplay="" loop="" muted="" class="video">
                    <source type="video/mp4" src="assets/images/video/csjd.mp4">
                </video>
                <div class="title-slider">
                    <img class="isotipo-home" src="assets/images/icons/isotipoB.svg" alt="">
                    <p class="text-title-slider titles-big text-uppercase">ayudamos a las<br>personas en la vida<br>y
                        su formación</p>
                </div>
            </div>
            <div class="item hg-100">
                <div class="degrade-int"></div>
                <img src="assets/images/noticia1.jpg" alt="">
                <div class="title-slider">
                    <img class="isotipo-home" src="assets/images/icons/isotipoB.svg" alt="">
                    <p class="text-title-slider titles-big text-uppercase">ayudamos a las<br>personas en la vida<br>y
                        su formación</p>
                </div>
            </div>
            <div class="item hg-100">
                <img src="assets/images/noticia1.jpg" alt="">
                <div class="title-slider">
                    <img class="isotipo-home" src="assets/images/icons/isotipoB.svg" alt="">
                    <p class="text-title-slider titles-big text-uppercase">ayudamos a las<br>personas en la vida<br>y
                        su formación</p>
                </div>
            </div> 
        </div>
        <a href="#"><img class="arrow-ancla" src="assets/images/icons/arrowAncla.svg" alt=""></a>
    </section>
    <!--SECCION CENTROS -->
    <section class="section sct-centers">
        <div class="container-fluid pd-x-0">
            <div class="row">
                <div class="wrapper-img-centers">
                    <!--CENTROS PERU-->
                    <div class="grid-centers">
                        <figure class="effect-julia">
                            <img class="img-bg-centers" src="assets/images/paises/peru.jpg" alt="" />
                            <figcaption>
                                <div class="imgMap-centers">
                                    <img src="assets/images/paises/map_Peru.svg" alt="" class="img-map">
                                    <div class="div-info-centers">
                                        <h2 class="title-text text-uppercase titles-big"><i class="icon-hojasLeft"></i> Perú <i class="icon-hojasRight"></i></h2>
                                        <button class="text-uppercase p-internas btn-centers hidden-xs hidden-sm">centros</button>
                                        <!-- SELECT MOBILE -->
                                        <select class="select-centers visible-xs visibles-sm">
                                            <option selected>CENTROS</option>
                                            <option value="">Complejo sanitario, San Juan de Dios, Piura</option>
                                            <option value="">Clínica San Juan de Dios, Arequipa</option>
                                            <option value="">Clínica San Juan de Dios, Chiclayo</option>
                                            <option value="">Clínica San Juan de Dios, Cusco</option>
                                            <option value="">Clínica San Juan de Dios, Iquitos</option>
                                            <option value="">Clínica San Juan de Dios, Lima</option>
                                            <option value="">Clínica San Juan de Dios, Piura</option>
                                            <option value="">Colegio Especializado CRIP, San Juan de Dios, Arequipa</option>
                                            <option value="">Hotel San Juan de Dios, Cusco</option>
                                        </select>
                                        <!-- fin select mobile -->
                                    </div>
                                </div>
                                <div class="list-centers" >
                                    <a class="link-list" href="#">Complejo sanitario, San Juan de Dios, Piura</a>
                                    <a class="link-list" href="#">Clínica San Juan de Dios, Arequipa</a>
                                    <a class="link-list" href="#">Clínica San Juan de Dios, Chiclayo</a>
                                    <a class="link-list" href="#">Clínica San Juan de Dios, Cusco</a>
                                    <a class="link-list" href="#">Clínica San Juan de Dios, Iquitos</a>
                                    <a class="link-list" href="#">Clínica San Juan de Dios, Lima</a>
                                    <a class="link-list" href="#">Clínica San Juan de Dios, Piura</a>
                                    <a class="link-list" href="#">Colegio Especializado CRIP, San Juan de Dios,
                                        Arequipa</a>
                                    <a class="link-list" href="#">Hotel San Juan de Dios, Cusco</a>
                                </div>
                            </figcaption>
                            <div class="bg-black"></div>
                        </figure>
                    </div>
                </div>
                <div class="wrapper-img-centers">
                    <!--CENTROS ECUADOR-->
                    <div class="grid-centers">
                        <figure class="effect-julia">
                            <img class="img-bg-centers" src="assets/images/paises/Ecuador.jpg" alt="" />
                            <figcaption>
                                <div class="imgMap-centers">
                                    <img src="assets/images/paises/map_Ecuador.svg" alt="" class="img-map">
                                    <div class="div-info-centers">
                                        <h2 class="title-text text-uppercase titles-big"><i class="icon-hojasLeft"></i> ecuador <i class="icon-hojasRight"></i></h2>
                                        <button class="text-uppercase p-internas btn-centers hidden-xs hidden-sm">centros</button>
                                        <!-- SELECT MOBILE -->
                                        <select class="select-centers visible-xs visibles-sm">
                                            <option selected>CENTROS</option>
                                            <option value="">Albergue de San Juan de Dios – Quito</option>
                                            <option value="">Hospital Especializado San Juan de Dios – Quito</option>
                                        </select>
                                        <!-- fin select mobile -->
                                    </div>
                                </div>
                                <div class="list-centers">
                                    <a class="link-list" href="#">Albergue de San Juan de Dios – Quito</a>
                                    <a class="link-list" href="#">Hospital Especializado San Juan de Dios –
                                        Quito</a>
                                </div>
                            </figcaption>
                            <div class="bg-black"></div>
                        </figure>

                    </div>
                </div>
                <div class="wrapper-img-centers">
                    <!--CENTROS VENEZUELA-->
                    <div class="grid-centers">
                        <figure class="effect-julia">
                            <img class="img-bg-centers" src="assets/images/paises/venezuela.jpg" alt="" />
                            <figcaption>
                                <div class="imgMap-centers">
                                    <img src="assets/images/paises/map_Venezuela.svg" alt="" class="img-map">
                                    <div class="div-info-centers">
                                        <h2 class="title-text text-uppercase titles-big"><i class="icon-hojasLeft"></i> venezuela <i class="icon-hojasRight"></i></h2>
                                        <button class="text-uppercase p-internas btn-centers hidden-xs hidden-sm">centros</button>
                                        <!-- SELECT MOBILE -->
                                        <select class="select-centers visible-xs visible-sm">
                                            <option selected>CENTROS</option>
                                            <option value="">Hogar Clínica San Rafael – SJD - Maracaibo</option>
                                            <option value="">Hospital San Juan de Dios – Caracas</option>
                                            <option value="">Hospital San Juan de Dios – Mérida</option>
                                        </select>
                                        <!-- fin select mobile -->
                                    </div>
                                </div>
                                <div class="list-centers">
                                    <a class="link-list" href="#">Hogar Clínica San Rafael – SJD - Maracaibo</a>
                                    <a class="link-list" href="#">Hospital San Juan de Dios – Caracas</a>
                                    <a class="link-list" href="#">Hospital San Juan de Dios – Mérida</a>
                                </div>
                            </figcaption>
                            <div class="bg-black"></div>
                        </figure>

                    </div>
                </div>
                <div class="wrapper-img-centers">
                    <!--CENTROS MUNDO-->
                    <div class="grid-centers">
                        <figure class="effect-julia">
                            <img class="img-bg-centers" src="assets/images/paises/Mundo.jpg" alt="" />
                            <figcaption>
                                <div class="imgMap-centers">
                                    <img src="assets/images/paises/map_Mundo.svg" alt="" class="img-map">
                                    <div class="div-info-centers">
                                        <h2 class="title-text text-uppercase titles-big"><i class="icon-hojasLeft"></i> mundo <i class="icon-hojasRight"></i></h2>
                                        <button class="text-uppercase p-internas btn-centers hidden-xs hidden-sm">centros</button>
                                        <!-- SELECT MOBILE -->
                                        <select class="select-centers visible-xs visible-sm">
                                            <option selected>CENTROS</option>
                                            <option value="">Curia General (Roma)</option>
                                            <option value="">Provincia Australiana</option>
                                            <option value="">Provincia Austríaca</option>
                                            <option value="">Provincia Bavaresa</option>
                                            <option value="">Provincia Bética</option>
                                            <option value="">Provincia Colombiana</option>
                                            <option value="">Provincia de Aragon - San Rafael</option>
                                            <option value="">Provincia de los Estados Unidos</option>
                                            <option value="">Provincia de México y América Central</option>
                                            <option value="">Provincia Francesa</option>
                                            <option value="">Provincia Inglesa</option>
                                            <option value="">Provincia Irlandesa</option>
                                            <option value="">Provincia Lombardo Véneta</option>
                                            <option value="">Provincia Romana</option>
                                            <option value="">Provincia Sudamericana Meridional</option>
                                            <option value="">Provincia Sudamericana Septentrional</option>
                                        </select>
                                        <!-- fin select mobile -->
                                    </div>
                                </div>
                                <div class="list-centers">
                                    <a class="link-list" href="#">Curia General (Roma)</a>
                                    <a class="link-list" href="#">Provincia Australiana</a>
                                    <a class="link-list" href="#">Provincia Austríaca</a>
                                    <a class="link-list" href="#">Provincia Bavaresa</a>
                                    <a class="link-list" href="#">Provincia Bética</a>
                                    <a class="link-list" href="#">Provincia Colombiana</a>
                                    <a class="link-list" href="#">Provincia de Aragon - San Rafael</a>
                                    <a class="link-list" href="#">Provincia de los Estados Unidos</a>
                                    <a class="link-list" href="#">Provincia de México y América Central</a>
                                    <a class="link-list" href="#">Provincia Francesa</a>
                                    <a class="link-list" href="#">Provincia Inglesa</a>
                                    <a class="link-list" href="#">Provincia Irlandesa</a>
                                    <a class="link-list" href="#">Provincia Lombardo Véneta</a>
                                    <a class="link-list" href="#">Provincia Romana</a>
                                    <a class="link-list" href="#">Provincia Sudamericana Meridional</a>
                                    <a class="link-list" href="#">Provincia Sudamericana Septentrional</a>
                                </div>
                            </figcaption>
                            <div class="bg-black"></div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SECCION NOVEDADES-->
    <section class="section sct-news bgInternas">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ttl-news color-primary text-center titles-big"><h2>NOVEDADES</h2></div>
                <div class="col-xs-6 card-big">
                    <div class="carrousel-news owl-carousel">
                        <div class="item-carrousel-news">
                            <div class="grid-news hg-100">
                                <div class="sombra"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/mundo.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia General
                                                - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="item-carrousel-news">
                            <div class="grid-news hg-100">
                                <div class="sombra"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/internas/mensajeros1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/mundo.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia General
                                                - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="item-carrousel-news">
                            <div class="grid-news hg-100">
                                <div class="sombra"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/internas/mensajeros2.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/mundo.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia General
                                                - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 card-peqs">
                    <div class="row">
                        <div class="col-xs-6 news-card-peq">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/ecuador.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-6 news-card-peq">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/venezuela.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-6 news-card-peq">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/mundo.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-6 news-card-peq">
                            <div class="grid-news">
                                <div class="sombra2"></div>
                                <figure class="wrapper-news">
                                    <img class="fondo-image" src="assets/images/noticia1.jpg" alt="img18" />
                                    <figcaption>
                                        <div class="title-news flex">
                                            <img class="img-circle" src="assets/images/icons/peru.svg" alt="">
                                            <p class="title-text">Encuentro de Superiores Provinciales en Curia
                                                General - Roma</p>
                                        </div>
                                        <a href="detalle-novedades.php"></a>
                                    </figcaption>
                                    <div class="vm"></div>
                                </figure>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SECCION FORMULARIO-->
    <section class="section form-and-part">
        <?php
            include 'src/includes/forms.php'
        ?>
        <!-- MODAL POLITICAS DE PRIVACIDAD -->
            <?php
                include 'src/includes/modal-pol-priv.php'
            ?>
        <!--SECCION PARTICIPA-->
        <?php
            include 'src/includes/sct-hazte.php'
        ?>
    </section>
    
    <?php
        include 'src/includes/footer.php'
    ?>
</main>
<script src="assets/js/libraries/fullpage.js"></script>
<script src="assets/js/modal-form.js"></script>
<script src="assets/js/libraries/jquery.validate.min.js"></script>
<script src="assets/js/form-validate.js"></script>
<script>
    if (screen && screen.width > 1300) {
        let fullpageDiv = $('#fullpage');
        if (fullpageDiv.length) {
            fullpageDiv.fullpage({
                scrollBar: true,
                scrollOverflow: true,
                verticalCentered: true,
                navigation: true,
                navigationPosition: 'right',
                navigationTooltips: ['Inicio', 'Red de Salud', 'Noticias', 'Contacto', 'Footer'],
                afterRender: function () {

                }
            });
        }
    };
    
</script>
<script>
    if (screen && screen.width > 575) {
        jQuery('a').click(function(){
        jQuery('html, body').animate({
            scrollTop: jQuery( jQuery(this).attr('href') ).offset().top
        }, 500);
        return false;
    });
    }
    

    
</script>
</body>

</html>