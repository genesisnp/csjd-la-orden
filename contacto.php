<?php
    include 'src/includes/header.php'
?>
<main>
    <!--BANNER-->
    <section class="sct-banner scroll" id="parallax">
        <div class="degrade-int"></div>
        <img class="img-banner" src="assets/images/banner/contacto.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase wow fadeInLeft">Contacto</h1>
        </div>
    </section>
    <section class="sct-contact-us">
        <?php
            include 'src/includes/forms.php'
        ?>
        <!-- MODAL POLITICAS DE PRIVACIDAD -->
        <?php
            include 'src/includes/modal-pol-priv.php'
        ?>
    </section>
    
</main>
<?php
    include 'src/includes/footer.php'
?>
<script src="assets/js/modal-form.js"></script>
<script src="assets/js/libraries/jquery.validate.min.js"></script>
<script src="assets/js/form-validate.js"></script>
</body>

</html>