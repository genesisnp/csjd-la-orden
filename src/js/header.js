$(document).ready(function () {
  // Header Scroll
  $(window).on('scroll', function () {
    const scroll = $(window).scrollTop();
    if (scroll >= 70) {
      $('#wrapper-header').addClass('fixed');
    } else {
      $('#wrapper-header').removeClass('fixed');
    }
  });
  
  const menuItems = document.querySelectorAll('.nav-big .item-nav-big');
  const menuImage = document.querySelector('.img-oh-big .img-cover');

  menuItems.forEach((menuItem, index) => {
    menuItem.addEventListener('mouseenter', () => {
      const src = menuItem.dataset.src;
      if (src) {
        console.log("Aqui", src, menuImage);
        menuImage.src = src;
      }
    });
  });

  $(".menuOpen").click(function () {
    $(".header").toggle();
  });

});
