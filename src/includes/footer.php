<div class="wrapper-footer section fp-auto-height">
    <footer class="pos-rel">
        <!-- <div class="infoMobile d-flex flex-column text-center">
            <h1 class="csjdF-title text-uppercase titlesBig hidden-lg">clínica san juan de dios arequipa</h1>
        </div> -->
        <div class="container pd-x-0">
            <div class="row d-flex jContentB">
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item"><a class="menuF-link text-uppercase">RED DE SALUD</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Perú</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Venezuela</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Ecuador</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item"><span class="menuF-link text-uppercase">la orden hospitalaria</span>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="nuestro-fundador.php" class="submenuF-link">Nuestro
                                    fundador</a>
                            </li>
                            <li class="submenuF-item"><a href="mision-y-vision.php" class="submenuF-link">Historia de la OH</a></li>
                            <li class="submenuF-item"><a href="mision-y-vision.php" class="submenuF-link">Misión y Visión</a></li>
                            <li class="submenuF-item"><a href="principios-valores.php" class="submenuF-link">Principios y Valores</a>
                            </li>
                            <li class="submenuF-item"><a href="estructura-organica.php" class="submenuF-link">Estructura Orgánica</a>
                            </li>
                            <li class="submenuF-item"><a href="nuestro-directorio.php" class="submenuF-link">Directorio</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Documentos de la OH</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item"><a class="menuF-link text-uppercase">¿QUE HACEMOS?</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Red de salud</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Social</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Educación y docencia</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Investigación</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Fundaciones</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link">Otros servicios</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item"><span class="menuF-link text-uppercase">formación</span>
                        <ul class="submenuF">
                            <li class="submenuF-item"><a href="pastoral-de-la-salud.php" class="submenuF-link">Pastoral de la salud y
                                    social</a></li>
                            <li class="submenuF-item"><a href="escuela-de-hospitalidad.php" class="submenuF-link">Escuela de
                                    hospitalidad</a></li>
                            <li class="submenuF-item"><a href="etica-y-bioetica.php" class="submenuF-link">Ética y bioética</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item"><a href="vocaciones-hospitalarias.php" class="menuF-link text-uppercase">vocaciones hospitalarias</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><span class="submenuF-link">Ser hermano de SJD</span>
                            </li>
                            <li class="submenuF-item"><span class="submenuF-link">Aspirantado</span></li>
                            <li class="submenuF-item"><span class="submenuF-link">Postulantado</span></li>
                            <li class="submenuF-item"><span class="submenuF-link">Noviciado</span></li>
                            <li class="submenuF-item"><span class="submenuF-link">Escolasticado</span></li>
                        </ul>
                    </li>
                </ul>
                <ul class="menuF-list visible-lg">
                    <li class="menuF-item"><a href="unete-a-nosotros.php" class="menuF-link text-uppercase">únete a nosotros</a>
                        <ul class="submenuF">
                            <li class="submenuF-item"><span class="submenuF-link">Ser hermano de San Juan</span></li>
                            <li class="submenuF-item"><span class="submenuF-link">Ser voluntario</span></li>
                            <li class="submenuF-item"><span class="submenuF-link">Ser bienhechor</span></li>
                            <li class="submenuF-item"><span class="submenuF-link">Bolsa laboral</span></li>
                        </ul>
                    </li>
                </ul>
                <ul class="menuF-list li-mobil">
                    <li class="menuF-item"><a href="contacto.php" class="menuF-link text-uppercase visible-lg">Contáctanos</a>
                        <ul class="submenuF fa-uls">
                            <li class="submenuF-item"><a href="#" class="submenuF-link"><span class="fa-li"><i class="icon-phone-bg"></i></span>(+511) 319 1401</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link"><span class="fa-li"><i class="icon-ubication"></i></span>Av. Nicolás Arriola #3250 San Luis,Lima - Perú</a></li>
                            <li class="submenuF-item"><a href="#" class="submenuF-link"><span class="fa-li"><i class="icon-sobre"></i></span>comunicaciones@sanjuandediosoh.com</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="socialNetworksF">
                <a href="#" class="iconF"><span class="icon-face"></span></a>
                <a href="#" class="iconF"><span class="icon-youtube"></span></a>
                <a href="#" class="iconF"><span class="icon-instagram"></span></a>
                <a href="#" class="iconF"><span class="icon-twitter"></span></a>
                <a href="#" class="iconF"><span class="icon-linkedin"></span></a>
            </div>
        <div class="credits">
            <div class="content-info-credits">
                <div>
                    <p class="mb-0"><a class="link-data d-none d-md-block" href="#">ORDEN HOSPITALARIA SAN JUAN DE DIOS</a></p>
                    <p class="mb-0">TODOS LOS DERECHOS RESERVADOS 2019</p>
                </div>
                <div class="content-htcss">
                    <p class="mb-0">POWERED BY <a class="link-data" href="http://exe.pe/">EXE.PE</a></p>
                    <p class="mb-0 hidden-xs hidden-sm">
                        <a class="link-data" href="https://validator.w3.org/check?uri=referer"
                            target="_blank">HTML</a>
                        •
                        <a class="link-data" href="https://jigsaw.w3.org/css-validator/check/referer"
                            target="_blank">CSS</a>
                    </p>
                </div>
            </div>

        </div>
    </footer>
</div>
<script src="assets/js/app.js"></script>
<script src="assets/js/libraries/wow.min.js"></script>

<script>
    var wow = new WOW({
        boxClass:     'wow',      
        animateClass: 'animated',
        offset:       0,    
        mobile:       false,       
        live:         true,       
        callback:     function(box) {
        },
        scrollContainer: null, 
        resetAnimation: true,    
    });
    wow.init();
</script>