<section class="sct-participate">
    <div class="container pd-x-0">
        <div class="row">
            <div class="col-xs-12 col-md-4 flex div-participate wow zoomIn">
                <div class="icon-container float-left">
                    <i class="icon-prtcp icon-hazte-voluntario"></i>
                </div>
                <div class="titles-participate float-left">
                    <h1 class="internas-bold text-uppercase">Hazte voluntario</h1>
                    <p class="p-internas">Participa activamente de nuestros programas sociales</p>
                    <div class="content-btn-participate">
                        <a href="#" class="btn btn-participate p-internas-bold shine btn-blueLightHover">PARTICIPA</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 flex div-participate wow zoomIn" data-wow-delay="1s">
                <div class="icon-container float-left">
                    <i class="icon-prtcp icon-hazte-bienhechor"></i>
                </div>
                <div class="titles-participate float-left">
                    <h1 class="internas-bold text-uppercase">Hazte bienhechor</h1>
                    <p class="p-internas">Ayúdanos económica, material y/o espiritualmente</p>
                    <div class="content-btn-participate">
                        <a href="#" class="btn btn-participate p-internas-bold shine btn-blueLightHover">PARTICIPA</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 flex div-participate wow zoomIn" data-wow-delay="2s">
                <div class="icon-container float-left">
                    <i class="icon-prtcp icon-hazte-hermano"></i>
                </div>
                <div class="titles-participate float-left">
                    <h1 class="internas-bold text-uppercase">Hazte hermano de San Juan de Dios</h1>
                    <p class="p-internas">Forma parte de nuestra gran familia</p>
                    <div class="content-btn-participate">
                        <a href="#" class="btn btn-participate p-internas-bold shine btn-blueLightHover">PARTICIPA</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>