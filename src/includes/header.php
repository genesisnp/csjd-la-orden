<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="assets/images/logos/LOGOPE.ico" rel="shortcut icon" />
    <title>ORDEN HOSPITALARIA SAN JUAN DE DIOS</title>
    <link rel="stylesheet" href="assets/css/app.css">
</head>

<body>
    <?php 
        $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));    
    ?>
    <div class="wrapper-header pos-fix" id="wrapper-header">
        <header class="header container pos-rel">
            <div class="contentLogo">
                <a href="index.php"><img class="logoAqpB" src="assets/images/logos/logoB.png" alt=""></a>
                <a href="index.php"><img class="logoAqp-hover" src="assets/images/logos/logoC.png" alt=""></a>
            </div>
            <div class="content-menuPrincipal">
                <div class="phones-and-socialH">
                    <a href="red-hospitalaria.php" class="d-inBlock font-semi-bold red-hosp shine btn-redHover"><i class="icon-red-salud"></i>RED HOSPITALARIA</a>
                    <a href="#" class="d-inBlock quotes phoneInfoForm"><i class="icon-phone"></i> <span
                            class="internas-bold ">(+511) 319 1401</span></a>
                    <div class="container-socialH d-inBlock dNone1024">
                    <a href="#" class="icon-socialH"><span class="icon-face"></span></a>
                    <a href="#" class="icon-socialH"><span class="icon-youtube"></span></a>
                    <a href="#" class="icon-socialH"><span class="icon-instagram"></span></a>
                    <a href="#" class="icon-socialH"><span class="icon-twitter"></span></a>
                    <a href="#" class="icon-socialH"><span class="icon-linkedin"></span></a>
                    </div>
                </div>
                <ul class="navbarList">
                    <li class="navbarItem dNone1024 <?= (in_array('nuestro-fundador.php', $uriSegments ) 
                                                        or in_array('historia-de-la-oh.php', $uriSegments )
                                                        or in_array('mision-y-vision.php', $uriSegments )
                                                        or in_array('principios-y-valores.php', $uriSegments )
                                                        or in_array('nuestro-directorio.php', $uriSegments ))
                                                        ? 'active' : ''; ?>"><a class="navbarLink" href="nuestro-fundador.php"><i class="icon-hojasLeft"></i> LA ORDEN HOSPITALARIA <i class="icon-hojasRight"></i></a></li>
                    <li class="navbarItem dNone1024 <?= in_array('vocaciones-hospitalarias.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink" href="vocaciones-hospitalarias.php"><i class="icon-hojasLeft"></i> VOCACIONES HOSPITALARIAS <i class="icon-hojasRight"></i></a></li>
                    <li class="navbarItem dNone1024 <?= in_array('contacto.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navbarLink" href="contacto.php"><i class="icon-hojasLeft"></i> CONTÁCTENOS <i class="icon-hojasRight"></i></a></li>
                </ul>
            </div>
            
        </header>
        <div id="menuArea">
            <input type="checkbox" id="menuToggle">

            <label for="menuToggle" class="menuOpen">
                <span class="open"></span>
            </label>

            <div class="menu menuEffects">
                <label for="menuToggle"></label>
                <div class="menuContent">
                    <nav class="navigation__nav">
                        <div class="navigation__list container-fluid pd-x-0">
                            <div class="nav-left col-xs-12 col-md-4 pd-x-0">
                                <a href="index.php" class="hidden-xs hidden-sm"><img class="logoAqp-hoverMenu" src="assets/images/logos/logoC.png" alt=""></a>
                                <div><a href="#" class="ttl-nav-left color-primary">REVISTA DIGITAL</a></div>
                                <div class="m-btn"><a href="red-hospitalaria.php" class="btn-r-hosp font-semi-bold shine btn-redHover"><i class="icon-red-salud"></i> RED HOSPITALARIA</a></div>
                                <div class="rs-nav-left">
                                    <a href="#" class="icon-nav-left icon-facebook-m"></a>
                                    <a href="#" class="icon-nav-left icon-youtube-m"></a>
                                    <a href="#" class="icon-nav-left icon-instagram-m"></a>
                                    <a href="#" class="icon-nav-left icon-twitter-m"></a>
                                    <a href="#" class="icon-nav-left icon-linkedin-m"></a>
                                </div>
                                <div class="phone-nav-left">
                                    <a href="tel:01-319-1401" class="color-primary"><i class="icon-phone"></i><span class="internas-bold ">(+511) 319 1401</span></a>
                                </div>
                                <p class="text-light-left mb-0">Av. Nicolás Arriola #3250 San Luis, Lima - Perú</p>
                                <a href="#" class="text-light-left">comunicaciones@sanjuandediosoh.com</a>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <div class="wrapper-nav-big row">
                                    <ul class="nav-big col-xs-12 col-md-6">
                                        <li class="item-nav-big" data-src="assets/images/header/img-header1.jpg">
                                            <span class="link-nav-big internas-bold">
                                                <i class="icon-hojas icon-hojasLeft"></i> la orden hospitalaria <i class="icon-hojas icon-hojasRight"></i>
                                            </span>
                                            <ul class="subm-nav-big">
                                                <li class="item-subm-big">
                                                    <a href="nuestro-fundador.php" class="link-subm-big">Nuestro fundador</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="historia-de-la-oh.php" class="link-subm-big">Historia de la OH</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="mision-y-vision.php" class="link-subm-big">Misión y visión</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="principios-y-valores.php" class="link-subm-big">Principios y valores</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="nuestro-directorio.php" class="link-subm-big">Directorio</a>
                                                </li>

                                            </ul>
                                        </li>  
                                        <li class="item-nav-big" data-src="assets/images/banner/contacto.jpg">
                                            <span class="link-nav-big internas-bold">
                                                <i class="icon-hojas icon-hojasLeft"></i> formacion <i class="icon-hojas icon-hojasRight"></i>
                                            </span>
                                            <ul class="subm-nav-big">
                                                <li class="item-subm-big">
                                                    <a href="pastoral-de-la-salud.php" class="link-subm-big">Pastoral de la Salud y Social</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="escuela-de-hospitalidad.php" class="link-subm-big">Escuela de Hospitalidad</a>
                                                </li>
                                                <li class="item-subm-big">
                                                    <a href="etica-y-bioetica.php" class="link-subm-big">Ética y Bioética</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="item-nav-big">
                                            <a class="link-nav-big internas-bold" href="vocaciones-hospitalarias.php">
                                                <i class="icon-hojas icon-hojasLeft"></i> vocaciones hospitalarias <i class="icon-hojas icon-hojasRight"></i>
                                            </a>
                                        </li>
                                        <li class="item-nav-big">
                                            <a class="link-nav-big internas-bold" href="unete-a-nosotros.php">
                                                <i class="icon-hojas icon-hojasLeft"></i> únete a nosotros <i class="icon-hojas icon-hojasRight"></i>
                                            </a>
                                        </li>
                                        <li class="item-nav-big">
                                            <a class="link-nav-big internas-bold" href="novedades.php">
                                                <i class="icon-hojas icon-hojasLeft"></i> novedades <i class="icon-hojas icon-hojasRight"></i>
                                            </a>
                                        </li>
                                        <li class="item-nav-big contc"><a class="link-nav-big internas-bold" href="contacto.php"><i class="icon-hojas icon-hojasLeft"></i> contacto <i class="icon-hojas icon-hojasRight"></i></a></li>
                                    </ul>

                                    <div class="tab-content-imgs col-xs-6 hidden-xs">
                                        <div class="img-oh-big img-header-big">
                                            <img class="img-cover" src="assets/images/header/img-header1.jpg" alt="">
                                        </div>      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>