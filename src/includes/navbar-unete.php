<div class="navbarInterRight pt7 col-xs-3 visible-lg wow slideInRight">
    <div>
        <div class="pd-x-0 pb2">
            <h2 class="titles-navInterR">ÚNETE A<br>NOSOTROS</h2>
        </div>
        <ul class="navList-InterRight">
            <li class="navItem-InterRight <?= in_array('ser-hermano.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navLink-InterRight" href="nuestro-fundador.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-fundador iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">Ser hermano<br>de san juan</h2>
                </a></li>
            <li class="navItem-InterRight <?= in_array('ser-voluntario.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navLink-InterRight" href="historia-de-la-oh.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-hist-oh iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">Ser<br>voluntario</h2>
                </a></li>
            <li class="navItem-InterRight <?= in_array('ser-bienhechor.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navLink-InterRight" href="mision-y-vision.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-mis-vis iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">ser<br>bienhechor</h2>
                </a></li>
            <li class="navItem-InterRight <?= in_array('bolsa-laboral.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navLink-InterRight"
                    href="principios-y-valores.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-princ-valores iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">bolsa<br>laboral
                    </h2>
                </a></li>
        </ul>
    </div>
</div>