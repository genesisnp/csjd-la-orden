<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-banner scroll" id="parallax">
        <div class="degrade-int"></div>
        <img class="img-banner" src="assets/images/banner/red.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase wow fadeInLeft">LA ORDEN HOSPITALARIA</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip wow fadeInLeft"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                principios</span><br>y valores</h2>
                                    </div>
                                    <div class="col-xs-12 pd-x-0">
                                        <div class="row content-mv pr-3">
                                            <div class="col-xs-12 col-sm-5 col-md-4 wrapper-mv">
                                                <div class="row">
                                                    <div class="mission wow fadeInLeft">
                                                        <h2 class="sub-ttl-flotant color-primary">Valores</h2>
                                                        <p class="text-p2 text-justify">La hospitalidad es nuestro valor central, 
                                                            que se expresa y se concreta en los cuatro valores guía que seguimos como institución.</p>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-8">
                                                <div class="img-mv-oh">
                                                    <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/principios.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- NUESTROS VALORES -->
                            <div class="col-xs-12 margin-t-varios">
                                <h2 class="sub-ttl-flotant ttl-presence color-primary wow fadeInLeft">Nuestros<br>valores como institución</h2>
                                <div class="wrapper-card-repeat">
                                    <div class="cols-value">
                                        <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/la-clinica/valor-calidad.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">calidad</p>
                                                    </div>
                                                </div>
                                                <div class="back" style="background-image: url(assets/images/internas/la-clinica/valor-calidad.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">calidad</p>
                                                        <p class="text-p2">La entendemos como la excelencia profesional en la atención integral al paciente, 
                                                            poniendo a su disposición los medios técnicos, humanos y espirituales que precise en cada momento.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');" data-wow-delay="1s">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/la-clinica/valor-respeto.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Respeto</p>
                                                    </div>
                                                </div>
                                                <div class="back" style="background-image: url(assets/images/internas/la-clinica/valor-respeto.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">respeto</p>
                                                        <p class="text-p2">Consideramos al usuario de nuestros Centros en su dimensión humana como el centro de nuestra atención, 
                                                            teniendo en cuenta sus derechos y decisiones e implicando en el proceso a los familiares. Promovemos la 
                                                            justicia social y los derechos civiles y humanos. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');" data-wow-delay="2s">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/la-clinica/valor-espiritualidad.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">espiritualidad</p>
                                                    </div>
                                                </div>
                                                <div class="back" style="background-image: url(assets/images/internas/la-clinica/valor-espiritualidad.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">espiritualidad</p>
                                                        <p class="text-p2">Acoge como valor el ofrecimiento de atención espiritual hacia todos los usuarios, pacientes, familiares y 
                                                            profesionales sea cual sea su confesionalidad considerando sus necesidades religiosas y contribuyendo, 
                                                            de esta manera, a la Evangelización.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');" data-wow-delay="3s">
                                            <div class="containers">
                                                <div class="front" style="background-image: url(assets/images/internas/la-clinica/valor-responsabilidad.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">Responsabilidad</p>
                                                    </div>
                                                </div>
                                                <div class="back" style="background-image: url(assets/images/internas/la-clinica/valor-responsabilidad.jpg)">
                                                    <div class="inner">
                                                        <p class="title-inner">responsabilidad</p>
                                                        <p class="text-p2">La asumimos como una obligación con los usuarios, el medio ambiente y hacia los ideales de San Juan de Dios y de la Orden. 
                                                            Siendo capaces, a su vez, de aplicar la ética y una justa distribución de los recursos de los que disponemos en todas las 
                                                            actividades que llevamos a cabo para la adecuada sostenibilidad de los Centros. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--NUESTROS PRINCIPIOS-->
                            <div class="col-xs-12 margin-t-varios">
                                <h2 class="sub-ttl-flotant ttl-presence color-primary wow fadeInLeft">Nuestros principios</h2>
                            </div>
                            <div class="col-xs-10 float-right pd-x-0">
                                <div class="wrapper-slide-beginning ">
                                    <div class="slide-beginning container-fluid pd-x-0 owl-carousel" id="slide-beginning">
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ1.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Afirmamos que el centro de interés es la persona asistida.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ2.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Promovemos y defendemos los derechos del enfermo y necesitado, teniendo en cuenta su dignidad.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ3.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Nos comprometemos en la defensa y promoción de la vida humana; Desde su concepción hasta la muerte.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ4.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Reconocemos el derecho de las personas asistidas a ser convenientemente informadas de su situación. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ5.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Impulsamos la asistencia integral, basada en el trabajo en equipo y el equilibrio entre técnica y humanización.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ6.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Observamos y promovemos los principios éticos de la iglesia católica.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ7.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Consideramos elemento esencial en la asistencia la dimensión espiritual y religiosa como oferta de curación y 
                                                    salvación, respetando otros credos y planteamientos de vida. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ8.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Defendemos el derecho a morir con dignidad y a que se respeten y atiendan los justos deseos de quienes están en trance de muerte.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ9.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Cuidamos la selección, formación y acompañamiento de los colaboradores, 
                                                    teniendo en cuenta su preparación, competencia profesional y sensibilización ante los valores 
                                                    y derechos de las personas. </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ10.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Observamos las exigencias del secreto profesional y tratamos de que sean respetadas.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ11.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Valoramos a los Colaboradores, haciéndoles partícipes de la misión de la Orden, 
                                                    en función de sus capacidades y áreas de responsabilidad.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ12.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Respetamos la libertad de conciencia y exigimos respeto a la identidad de los Centros.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item row">
                                            <div class="col-xs-12 slider-sub-item">
                                                <div class="img-slider-beginning">
                                                    <img src="assets/images/internas/la-clinica/slide-princ13.jpg" alt="">
                                                </div>
                                                <div class="description-item-beginning">
                                                    <p class="text-p2">Nos oponemos al afán de lucro; por tanto, observamos y exigimos que se respeten las normas económicas y retributivas justas.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="indicator"></div>
                                </div>
                            </div>
                        </div>
                        <!---->
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-oh.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>