<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-banner scroll" id="parallax">
        <div class="degrade-int"></div>
        <img class="img-banner" src="assets/images/banner/vocacionHospitalaria.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase wow fadeInLeft">vocaciones hospitalarias</h1>
        </div>
    </section>
    <!---->
    <section class="sct-hospital-vocation">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 pd-x-0">
                    <h2 class="titles-descrip wow fadeInLeft"><span class="icon-san"></span>
                        <span class="span-titlesDescrip">
                        ser religioso</span><br>de san juan dios</h2>
                </div>
                <div class="col-xs-12">
                    <div class="us-oh row">
                        <div class="img-us-oh float-right col-xs-12 col-md-10 pd-x-0">
                            <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/voc-hosp1.jpg" alt="">
                        </div>
                        <div class="description-flotant descp-flot-l dscp-2 wow fadeInLeft">
                            <p class="text-border text-p2">La Orden Hospitalaria la conformamos Hermanos consagrados a Dios, que 
                            seguimos a Jesús, al estilo de nuestro fundador San Juan de Dios. Por ello, vivimos en comunidad y nos
                             dedicamos a Evangelizar desde el carisma de la Hospitalidad, en el ámbito de la salud y poniéndonos 
                             al servicio de aquellos que experimentan pobreza, sufrimiento, marginación y otras carencias. 
                             Prolongando de esta manera el amor compasivo y misericordioso de Dios, entre los hombres.</p>
                        </div>
                    </div>
                </div>
                <!-- VOCACIÓN -->
                <div class="col-xs-12 pd-x-0 margin-dimens">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8">
                            <div class="content-img-pstoral float-left">
                                <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/voc-hosp2.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 descrip-formation wow fadeInRight">
                            <div class="row">
                                <div class="col-xs-12 pd-x-0">
                                    <h2 class="sub-ttl-flotant color-primary">Vocación</h2>
                                </div>
                                <p class="text-p2 text-justify">La vocación es iniciativa y un don recibido de Dios, pero es también una 
                                    tarea que nos compromete. El Señor nos amó y llamó primero, y nosotros le hemos respondido siguiéndolo. 
                                    Es por habernos sentido amados por Él, que hemos acogido su invitación anteponiendo a otros intereses 
                                    que el mundo nos ofrece. Orientamos nuestra vida para desarrollar un proyecto de vida personal y 
                                    comunitario de seguimiento a Jesús, desde el carisma de la Hospitalidad.</p>
                                <p class="text-p2 text-justify">Para nosotros los Religiosos de San Juan de Dios, la Hospitalidad se convierte 
                                    en una manera concreta de vivir y seguir a Jesús. Ser hospitalarios es todo un estilo de vida que nos impulsa 
                                    a acoger sin límites a todo ser humano de este mundo marcado por la hostilidad.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- san juan de dios -->
                <div class="col-xs-12 pd-x-0 margin-dimens">
                    <div class="row">
                        <div class="col-xs-12 col-md-4 descrip-formation wow fadeInRight">
                            <div class="row">
                                <div class="col-xs-12 pd-x-0">
                                    <h2 class="sub-ttl-flotant color-primary">San Juan de Dios</h2>
                                </div>
                                <p class="text-p2 text-justify">De Juan de Dios conservamos pocos escritos, pero en ellos él nos expresa la profundidad 
                                    y riqueza de su experiencia de Dios. Y nos invita a vivir esta misma experiencia y por ello nos dice: “Si conocieras 
                                    lo grande que es la misericordia de Dios, nunca dejarías de hacer el bien mientras pudieses”. La grandeza del amor de 
                                    Dios que lo impregna todo, nos conduce al amor mismo en nuestros hermanos necesitados. Por eso Juan nos llama a “Tener 
                                    siempre caridad, porque donde hay caridad, hay Dios, aunque Dios en todo lugar está”.</p>
                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="content-img-pstoral float-right">
                                <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/voc-hosp3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- programa de acompañamiento -->
                <div class="col-xs-12 padd-0 progr-acom">
                    <div class="us-oh">
                        <div class="img-us-oh float-left">
                            <img class="img-cover wow fadeInLeft" src="assets/images/internas/la-clinica/voc-hosp4.jpg" alt="">
                        </div>
                        <div class="description-flotant descp-flot-r wow fadeInRight">
                            <h2 class="sub-ttl-flotant color-primary">Programa de<br>acompañamiento vocacional</h2>
                            <ul class="list-progr-acom">
                                <li class="item-progr-acom text-p2">Encuentros y entrevistas personales con el Hermano Acompañante.</li>
                                <li class="item-progr-acom text-p2">Inserción periódica o permanente en la Comunidad asignada para esta experiencia.</li>
                                <li class="item-progr-acom text-p2">Participación en convivencias vocacionales con otros aspirantes.</li>
                                <li class="item-progr-acom text-p2">Profundización de la vida cristiana y lectura de los documentos de la Iglesia y de la Orden.</li>
                                <li class="item-progr-acom text-p2">Compromiso con realidades de pobreza, enfermedad, sufrimiento y exclusión.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- acompañamiento vocacional interno -->
                <div class="col-xs-12 pd-x-0 margin-dimens">
                    <div class="row">
                        <div class="col-xs-12 col-md-4 descrip-formation wow fadeInRight">
                            <div class="row">
                                <div class="col-xs-12 pd-x-0">
                                    <h2 class="sub-ttl-flotant color-primary">Acompañamiento<br>vocacional interno</h2>
                                </div>
                                <p class="text-p2 text-justify">Esta etapa inicial es un tiempo caracterizado por el mutuo 
                                    conocimiento entre el candidato y la Orden. Es una experiencia en el que el joven discierne 
                                    si tiene cualidades para ser religioso. Para ello el candidato se inserta de manera periódica 
                                    o permanente en una Comunidad nuestra. Es un tiempo para revisar y profundizar en la propia 
                                    experiencia humana y cristiana, a través del compartir de la historia la historia personal e 
                                    historia de fe.</p>
                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="content-img-pstoral float-right">
                                <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/voc-hosp3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- PROCESO DE FORMACIÓN -->
                <div class="col-xs-12">
                    <h2 class="sub-ttl-flotant ttl-presence color-primary wow fadeInLeft">Proceso de Formación<br>del Hermano de San Juan de Dios</h2>
                </div>
                <div class="col-xs-12 pd-x-0">
                    <h2 class="sub-ttl-flotant color-primary">Formación Inicial</h2>
                </div>
                <div class="col-xs-12 card-process-formation pd-x-0">
                    <div class="wrapper-card-repeat">
                        <div class="cols-value">
                            <!-- <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 cols" ontouchstart="this.classList.toggle('hover');">
                                <div class="containers">
                                    <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-conocimiento.jpg)">
                                        <div class="inner">
                                            <i class="icon-card icon-aspirantado"></i>
                                            <p class="title-inner">aspirantado</p>
                                        </div>
                                    </div>
                                    <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-conocimiento.jpg)">
                                        <div class="inner">
                                            <p class="title-inner">aspirantado</p>
                                            <p class="text-p2">Esta etapa inicial es un tiempo caracterizado por el mutuo conocimiento entre el 
                                                candidato y la Orden. Es un período de formación en el que el joven discierne si tiene cualidades 
                                                para ser un Hermano. Es un tiempo para profundizar en la propia experiencia humana y cristiana, 
                                                dedicado a conocerse, aceptarse, amarse, convertirse al evangelio y demostrar que hay una 
                                                identificación con el carisma hospitalario.</p>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="wow zoomIn col-xs-12 col-sm-6 col-md-4 cols" ontouchstart="this.classList.toggle('hover');" data-wow-delay="1s">
                                <div class="containers">
                                    <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-identificacion.jpg)">
                                        <div class="inner">
                                            <i class="icon-card icon-postulantado"></i>
                                            <p class="title-inner">postulantado</p>
                                        </div>
                                    </div>
                                    <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-identificacion.jpg)">
                                        <div class="inner">
                                            <p class="title-inner">postulantado</p>
                                            <p class="text-p2">Esta etapa, también llamada pre-noviciado, permite continuar el discernimiento e 
                                                iniciar el proceso de formación como Hermano de San Juan de Dios. Es un período donde se favorece 
                                                el crecimiento del postulante en su dimensión humano-afectiva y vida de fe; e inicia una experiencia 
                                                en la vida consagrada en cuanto a: vida de oración, vida de comunidad, vida apostólica.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wow zoomIn col-xs-12 col-sm-6 col-md-4 cols" ontouchstart="this.classList.toggle('hover');" data-wow-delay="2s">
                                <div class="containers">
                                    <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-hospitalidad.jpg)">
                                        <div class="inner">
                                            <i class="icon-card icon-noviciado"></i>
                                            <p class="title-inner">noviciado</p>
                                        </div>
                                    </div>
                                    <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-hospitalidad.jpg)">
                                        <div class="inner">
                                            <p class="title-inner">noviciado</p>
                                            <p class="text-p2">Es una etapa fundamental, los novicios viven la experiencia del encuentro personal 
                                                con Dios, disciernen, clarifican y profundizan la llamada del Señor, para poder tomar libre y 
                                                conscientemente, la decisión de seguir a Cristo Buen Samaritano, en la Orden Hospitalaria, en 
                                                condiciones suficientes de estabilidad y equilibrio espiritual. Es un tiempo donde se evidencian 
                                                las cualidades humanas y espirituales de los novicios, comprobando su intención para la profesión 
                                                de los votos religiosos.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wow zoomIn col-xs-12 col-sm-6 col-md-4 cols" ontouchstart="this.classList.toggle('hover');" data-wow-delay="3s">
                                <div class="containers">
                                    <div class="front" style="background-image: url(assets/images/internas/la-clinica/obj-compromiso.jpg)">
                                        <div class="inner">
                                            <i class="icon-card icon-escolasticado"></i>
                                            <p class="title-inner">escolasticado</p>
                                        </div>
                                    </div>
                                    <div class="back" style="background-image: url(assets/images/internas/la-clinica/obj-compromiso.jpg)">
                                        <div class="inner">
                                            <p class="title-inner">escolasticado</p>
                                            <p class="text-p2">Etapa de formación teológica, profesional y consolidación vocacional, que abarca el 
                                                tiempo que va desde la primera profesión hasta la profesión solemne. En esta etapa se pretende 
                                                consolidar la opción vocacional, consiguiendo el grado de madurez que le permita comprender y vivir 
                                                su consagración en la Orden como un verdadero bien para sí mismo y para los demás: asimilando el 
                                                carisma con profundo espíritu evangélico.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- formacion permanente -->
                <div class="col-xs-12 pd-x-0 margin-dimens">
                    <div class="row">
                        <div class="col-xs-12 col-md-4 descrip-formation wow fadeInRight">
                            <div class="row">
                                <div class="col-xs-12 pd-x-0">
                                    <h2 class="sub-ttl-flotant color-primary">Formacion<br>permanente :</h2>
                                </div>
                                <p class="text-p2 text-justify">Todo religioso hospitalario de Votos Solemnes debe 
                                    mantenerse actualizado y capacitado para responder a las exigencias de toda Vida
                                     y Misión Hospitalaria. Por eso, aunque llegue a su término la formación inicial 
                                     el Hermano de San Juan de Dios ha de mantenerse siempre disponible a aprender 
                                     y adquirir nuevos conocimientos. </p>
                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="content-img-pstoral float-right">
                                <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/voc-hosp6.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ETAPAS DE CONOCIMIENTO -->
                <div class="col-xs-12 pd-x-0 description-etaps">
                    <div class="row">
                        <div class="col-xs-12 col-md-7 hidden-xs hidden-sm">
                            <div class="img-etapas">
                                <img class="img-cover wow fadeInUp" src="assets/images/internas/unete-a-us1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-5">
                            <h2 class="sub-ttl-flotant color-primary wow fadeInLeft">Etapas de conocimiento<br>y discernimiento</h2>
                            <div class="etaps etap-one wow fadeInLeft">
                                <h3 class="ttl-etaps color-primary font-semi-bold">Primera etapa</h3>
                                <p class="text-p2">Se caracteriza por ser un tiempo de “ver”, donde el joven expresa su interés 
                                    de conocer lo que hacemos, por lo cual se le invita a que participe de actividades del 
                                    voluntariado, visitas a alguna comunidad u obra nuestra, retiros juveniles y participación en 
                                    reuniones informativas del carisma hospitalario.</p>
                            </div>
                            <div class="etaps etap-two wow fadeInLeft">
                                <h3 class="ttl-etaps color-primary font-semi-bold">Segunda etapa</h3>
                                <p class="text-p2">La segunda etapa, partimos de la premisa de que el joven ya manifiesta expresamente 
                                    su decisión de comenzar un proceso formal de discernimiento a la Vida Religiosa y a la Orden 
                                    Hospitalaria de San Juan de Dios. En estos casos, el joven es considerado como Aspirante y se le 
                                    propone el Programa de Acompañamiento Vocacional.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="us-oh row">
                        <div class="img-us-oh float-right col-xs-12 col-md-10 pd-x-0">
                            <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/voc-hosp8.jpg" alt="">
                        </div>
                        <div class="description-flotant descp-flot-l dscp-2 wow fadeInLeft">
                            <p class="text-border text-p2">La Orden Hospitalaria la conformamos Hermanos consagrados a Dios, que 
                            seguimos a Jesús, al estilo de nuestro fundador San Juan de Dios. Por ello, vivimos en comunidad y nos
                             dedicamos a Evangelizar desde el carisma de la Hospitalidad, en el ámbito de la salud y poniéndonos 
                             al servicio de aquellos que experimentan pobreza, sufrimiento, marginación y otras carencias. 
                             Prolongando de esta manera el amor compasivo y misericordioso de Dios, entre los hombres.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!--FOOTER-->
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>