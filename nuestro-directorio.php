<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-banner scroll" id="parallax">
        <div class="degrade-int"></div>
        <img class="img-banner" src="assets/images/banner/red.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase wow fadeInLeft">LA ORDEN HOSPITALARIA</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip mb-0 wow fadeInLeft"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                            nuestro</span><br>directorio</h2>
                                    </div>
                                    <!-- DIRECTOR -->
                                    <div class="col-xs-12 padd-0 director pr-3">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="description-hrnos wow fadeInLeft">
                                                    <h2 class="name-hrno color-primary font-semi-bold">hno. William pintado saavedra</h2>
                                                    <h3 class="position-hrno color-secondary p-internas">superior provincial</h3>
                                                    <p class="text-p2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                                        Natus voluptas expedita dicta, aperiam fugit id, quos exercitationem, 
                                                        amet nulla iste aliquid minus? Enim veritatis praesentium a, odit molestiae vitae quas.</p>
                                                    <a href="#" class="email-hrno color-primary font-semi-bold">provincial-sep@sanjuandedios.pe</a>
                                                </div>
                                                
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-8">
                                                <div class="img-superior">
                                                    <img class="img-cover wow fadeInUp" src="assets/images/internas/la-clinica/superior-director.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 consejeros">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-10 col-md-6 wrapper-consejero">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="img-consejero">
                                                            <img class="img-cover wow zoomIn" src="assets/images/internas/la-clinica/consejero1.jpg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="description-hrnos wow fadeIn" data-wow-delay="1s">
                                                            <h2 class="name-hrno color-primary font-semi-bold">hno. césar arroyo gutierrez.OH</h2>
                                                            <h3 class="position-hrno color-secondary p-internas">1° consejero provincial</h3>
                                                            <a href="#" class="email-hrno color-primary font-semi-bold">provincial-sep@sanjuandedios.pe</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-10 col-md-6 wrapper-consejero">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="img-consejero">
                                                            <img class="img-cover wow zoomIn" src="assets/images/internas/la-clinica/consejero2.jpg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="description-hrnos wow fadeIn" data-wow-delay="1s">
                                                            <h2 class="name-hrno color-primary font-semi-bold">hno. josé daniel hernández parra.OH</h2>
                                                            <h3 class="position-hrno color-secondary p-internas">2° consejero provincial</h3>
                                                            <a href="#" class="email-hrno color-primary font-semi-bold">cesar.arroyo@sanjuandedios.pe</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-10 col-md-6 wrapper-consejero">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="img-consejero">
                                                            <img class="img-cover wow zoomIn" src="assets/images/internas/la-clinica/consejero3.jpg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="description-hrnos wow fadeIn" data-wow-delay="1s">
                                                            <h2 class="name-hrno color-primary font-semi-bold">hno. benigno chahuillco allahua.OH</h2>
                                                            <h3 class="position-hrno color-secondary p-internas">3° consejero provincial</h3>
                                                            <a href="#" class="email-hrno color-primary font-semi-bold">benigno.chahuillco@sanjuandedios.pe</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-10 col-md-6 wrapper-consejero">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="img-consejero">
                                                            <img class="img-cover wow zoomIn" src="assets/images/internas/la-clinica/consejero4.jpg" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="description-hrnos wow fadeIn" data-wow-delay="1s">
                                                            <h2 class="name-hrno color-primary font-semi-bold">hno. américo quispe merino.OH</h2>
                                                            <h3 class="position-hrno color-secondary p-internas">4° consejero provincial</h3>
                                                            <a href="#" class="email-hrno color-primary font-semi-bold">americo.quispe@sanjuandedios.pe</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-oh.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>